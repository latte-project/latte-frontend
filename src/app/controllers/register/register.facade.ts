import { Injectable } from '@angular/core';
import { IRegisterAccount } from '../../models/register/register.interface';
import { JwtService } from '../../services/utils/jwt.service';
import { RegisterService } from '../../services/register/register.service';
import { NbToastrService } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class RegisterFacade {
  constructor(
    private jwtService: JwtService,
    private registerService: RegisterService,
    private toastrService: NbToastrService,
  ) {}

  registerNewAccount(registerAccountCtx: IRegisterAccount): void {
    const tokenizedPayload = this.jwtService.getToken(registerAccountCtx);
    console.log(tokenizedPayload);
    this.registerService.registerNewAccount(tokenizedPayload).subscribe(
      res => {
        if (res.status === 'OK') {
          this.toastrService.success(res.message, 'Success');
        }
        if (res.status === 'FAIL') {
          this.toastrService.danger(res.message, 'Failed');
        }
      },
      err => {
        console.error(err);
      }
    );
  }
}
