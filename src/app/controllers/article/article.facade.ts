import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ArticleFacade {
  createArticleForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.createArticleForm = this.formBuilder.group({
      title: [''],
      content: [''],
      banner: [''],
      tags: [{
        news: false,
        hot: false,
        event: false,
        guide: false,
        etc: true,
      }],
    });
  }

  getCreateArticleForm(): FormGroup {
    return this.createArticleForm;
  }
}
