import { Injectable } from '@angular/core';
import { UserService } from '@services/user/user.service';
import { AuthenticationService } from '../../services/login/authentication.service';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { JwtService } from '@app/services/utils/jwt.service';
import { NbToastrService } from '@nebular/theme';
import { Subject, of, Observable, throwError } from 'rxjs';
import { takeUntil, catchError, map, concatMap } from 'rxjs/operators';
import { IAccessToken } from '@app/models/login/login.interface';
import { SourceOfTruthService } from '@app/state/source-of-truth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginFacade {
  destroy$: Subject<boolean> = new Subject<boolean>();
  toastrTitle = 'Login';
  loginForm: FormGroup = this.formBuilder.group({
    username: [''],
    password: [''],
  });

  constructor(
    private formBuilder: FormBuilder,
    private jwtService: JwtService,
    private toastrService: NbToastrService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private sotService: SourceOfTruthService,
  ) {}

  onLogin(payload: any): Observable<any> {
    const encodedPayload = this.jwtService.encode(payload);
    const login1stStep$ = this.getRefreshToken(encodedPayload);
    const login2ndStep$ = this.getAccessToken();
    const login3rdStep$ = this.getUserInfo();
    const loginFullStep$ = login1stStep$.pipe(
      concatMap(step1 => login2ndStep$),
      concatMap(step2 => login3rdStep$),
    );
    return loginFullStep$;
  }

  getRefreshToken(payload: string): Observable<any> {
    return this.userService.login(payload).pipe(
      takeUntil(this.destroy$),
      map(
        (res) => {
          console.log(res);
          console.log('login 1st step');
          this.toastrService.success('Đăng nhập thành công', this.toastrTitle);
        }
      ),
      catchError(
        (err) => {
          console.error(err);
          this.toastrService.danger(err.error.message, this.toastrTitle);
          return throwError(new Error(`getRefreshToken fail`));
        }
      )
    );
  }

  getAccessToken(): Observable<any> {
    return this.userService.getAccessToken().pipe(
      takeUntil(this.destroy$),
      map(
        (res: IAccessToken) => {
          console.log(res);
          console.log('login 2nd step');
          this.authenticationService.updateAccessToken(res);
          this.sotService.updateAccessToken(res);
        }
      ),
      catchError(
        (err) => {
          console.error(err);
          this.authenticationService.updateAccessToken(null);
          this.sotService.updateAccessToken(null);
          return throwError(new Error(`getAccessToken fail`));
        }
      )
    );
  }

  getUserInfo(): Observable<any> {
    return this.userService.getUserInfo().pipe(
      takeUntil(this.destroy$),
      map(
        (res) => {
          console.log(res);
          console.log('login 3rd step');
          this.sotService.updateUserInfo(res.payload);
        }
      ),
      catchError(
        (err) => {
          console.error(err);
          this.sotService.updateUserInfo(null);
          return throwError(new Error(`getUserInfo fail`));
        }
      )
    );
  }

  get username(): AbstractControl {
    return this.loginForm.get('username');
  }

  get password(): AbstractControl {
    return this.loginForm.get('password');
  }

  onDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
