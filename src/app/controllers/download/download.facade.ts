import { Injectable } from '@angular/core';
import { DownloadService } from '@services/download/download.service';

@Injectable({
  providedIn: 'root'
})
export class DownloadFacade {
  constructor(
    private downloadService: DownloadService
  ) {}

  getLinkFull(): string {
    return this.downloadService.getLinkFull();
  }

  getLinkPatch(): string {
    return this.downloadService.getLinkPatch();
  }
}
