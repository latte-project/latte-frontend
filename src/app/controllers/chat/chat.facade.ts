import { Injectable } from '@angular/core';
import { IChatMessage } from '../../models/chat/chat.interface';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { NavbarFacade } from '../navbar/navbar.facade';

@Injectable({
  providedIn: 'root'
})
export class ChatFacade {
  chatWs: WebSocketSubject<unknown>;
  constructor(
    private navbarFacade: NavbarFacade,
  ) {}

  sendMessage(mes: IChatMessage): void {
    this.chatWs.next(mes);
  }

  init(): WebSocketSubject<unknown> {
    const ws = webSocket(`ws://localhost:8080`);
    this.chatWs = ws;
    return ws;
  }
}
