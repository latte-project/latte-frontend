import { Injectable } from '@angular/core';
import { NavbarService } from '../../services/navbar/navbar.service';
import { Observable, concat } from 'rxjs';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';
import { NbMenuService } from '@nebular/theme';
import { filter, map, concatMap } from 'rxjs/operators';
import { SourceOfTruthService } from '@app/state/source-of-truth.service';
import { LoginFacade } from '../login/login.facade';
import { UserService } from '@app/services/user/user.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavbarFacade {
  constructor(
    private navbarService: NavbarService,
    private sotQuery: SourceOfTruthQuery,
    private menuService: NbMenuService,
    private sotService: SourceOfTruthService,
    private loginFacade: LoginFacade,
    private userService: UserService,
    private route: Router,
  ) {}

  onInit(): void {
    this.sotQuery.select('apiEndpoint').subscribe(
      (val) => {
        if (val) {
          const steps = concat(
            this.loginFacade.getAccessToken(),
            this.loginFacade.getUserInfo(),
          );
          steps.subscribe();
        }
      }
    );
  }

  getNavbarMeta(): Observable<any> {
    return this.navbarService.getNavbarMeta();
  }

  getUserInfo(): Observable<any> {
    return this.sotQuery.select('userInfo');
  }

  handleUserMenu(): void {
    this.menuService.onItemClick().pipe(
      filter(
        ({ tag }) => tag === 'l-user-menu'
      ),
      map(
        ({ item }) => item
      ),
    ).subscribe(
      (res: any) => {
        console.log(res);
        if (res.ref === 'logout') {
          this.logout();
        }
        if (res.ref === 'newarticle') {
          this.route.navigate(['article/create']);
        }
      },
      err => {
        console.error(err);
      }
    );
  }

  logout(): void {
    this.userService.logout().subscribe(res => {
      console.log(res);
    });
    this.sotService.updateAccessToken(null);
    this.sotService.updateUserInfo(null);
  }
}
