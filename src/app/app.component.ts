import { Component, OnInit } from '@angular/core';
import { SourceOfTruthService } from './state/source-of-truth.service';
import { SourceOfTruthQuery } from './state/source-of-truth.query';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private sourceOfTruthService: SourceOfTruthService,
  ) {
    // this.sourceOfTruthService.init();
  }

  async ngOnInit(): Promise<void> {
    await this.sourceOfTruthService.init();
  }
}
