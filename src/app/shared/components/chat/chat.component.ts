import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation, Input, ChangeDetectorRef } from '@angular/core';
import { NavbarFacade } from '@app/controllers/navbar/navbar.facade';
import { IChatMessage } from '@app/models/chat/chat.interface';
import { ChatFacade } from '@controllers/chat/chat.facade';
import { Observable } from 'rxjs';
import { WebSocketSubject } from 'rxjs/webSocket';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class ChatComponent implements OnInit {
  chatWsSubject: WebSocketSubject<unknown>;
  date = new Date();
  @Input() title = 'Main';
  @Input() messages = [
    {
      date: new Date(),
      dateFormat: 'HH:mm MMM dd yyyy',
      files: [],
      latitude: null,
      longitude: null,
      message: 'Default message',
      quote: false,
      sender: 'Quidapchai',
      type: 'text',
      avatar: 'https://i.gifer.com/no.gif',
      reply: false,
    }
  ];


  constructor(
    private chatFacade: ChatFacade,
    private navbarFacade: NavbarFacade,
    private cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.chatWsSubject = this.chatFacade.init();
    this.chatWebsocketHandler();
  }

  sendMessage({ message, files }: { message: string, files: any[] }): void {
    const mesCtx: IChatMessage | any = {
      message,
      dateFormat: 'HH:mm MMM dd yyyy',
      files: [],
      latitude: null,
      longitude: null,
      date: new Date(),
      sender: 'Qui Nguyen',
      avatar: 'https://i.gifer.com/no.gif',
      type: 'text',
    };
    this.messages.push(mesCtx);
    this.chatFacade.sendMessage(mesCtx);
  }

  chatWebsocketHandler(): void {
    this.chatWsSubject.subscribe(
      (res: IChatMessage | any) => {
        this.messages.push(res);
        this.cdr.detectChanges();
      },
      err => {
        console.error(err);
      }
    );
  }

  get userMeta$(): Observable<any> {
    return this.navbarFacade.getUserInfo();
  }

}
