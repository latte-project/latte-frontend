import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, EventEmitter, Output } from '@angular/core';
import { LoginFacade } from '@app/controllers/login/login.facade';
import { FormGroup } from '@angular/forms';
import { AuthenticationService } from '@app/services/login/authentication.service';
import { Observable } from 'rxjs';
import { IAccessToken } from '@app/models/login/login.interface';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
  @Output() loginEvent: EventEmitter<any> = new EventEmitter(null);

  constructor(
    private loginFacade: LoginFacade,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    // this.loginFacade.onDestroy();
  }

  get loginForm(): FormGroup {
    return this.loginFacade.loginForm;
  }

  onLogin(loginForm: FormGroup): void {
    const loginFullStep$ = this.loginFacade.onLogin(this.loginForm.value);
    loginFullStep$.subscribe(
      res => {
        this.loginEvent.emit({ status: 'OK', message: 'login success '});
      },
      err => {
        console.error(err);
      },
    );
  }

  getAccessToken(): void {
    this.loginFacade.getAccessToken().subscribe();
  }

  get accessToken$(): Observable<IAccessToken> {
    return this.authenticationService.accessToken$;
  }

}
