import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit {
  @Input() title: string;
  @Input() link: string;
  @Input() text: string;
  @Input() banner = 'https://photos.animetvn.tv/upload/film_big/bWeTXxl.png';
  @Input() badges: any;
  @Input() createdAt: string;
  @Input() author: string;

  constructor() { }

  ngOnInit(): void {
  }

}
