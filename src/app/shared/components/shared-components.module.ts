import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import {
  NbCardModule,
  NbInputModule,
  NbButtonModule,
  NbContextMenuModule,
  NbUserModule,
  NbBadgeModule,
  NbChatModule
} from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { PostComponent } from './post/post.component';
import { BadgeComponent } from './badge/badge.component';
import { ChatComponent } from './chat/chat.component';


@NgModule({
  declarations: [
    NavbarComponent,
    LoginComponent,
    PostComponent,
    BadgeComponent,
    ChatComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NbCardModule,
    NbInputModule,
    NbButtonModule,
    ReactiveFormsModule,
    NbContextMenuModule,
    NbUserModule,
    NbBadgeModule,
    NbChatModule
  ],
  exports: [
    NavbarComponent,
    LoginComponent,
    PostComponent,
    BadgeComponent,
    ChatComponent
  ]
})
export class SharedComponentsModule { }
