import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NavbarFacade } from '@app/controllers/navbar/navbar.facade';
import { Observable } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NbMenuService } from '@nebular/theme';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {
  userMenuItems = [
    { title: 'Profile', ref: 'profile' },
    { title: 'Đăng bài viết', ref: 'newarticle'},
    { title: 'Logout', ref: 'logout' },
  ];
  loginModalRef: NgbModalRef;

  constructor(
    private navbarFacade: NavbarFacade,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.navbarFacade.onInit();
    this.handleUserMenu();
  }

  openModal(content: any): void {
    this.loginModalRef = this.modalService.open(content);
  }

  onLoginEvent(event: any): void {
    console.log(event);
    if (event.status === 'OK') {
      this.loginModalRef.close();
    }
  }

  get navbarMeta$(): Observable<any> {
    return this.navbarFacade.getNavbarMeta();
  }

  get userMeta$(): Observable<any> {
    return this.navbarFacade.getUserInfo();
  }

  handleUserMenu(): void {
    this.navbarFacade.handleUserMenu();
  }
}
