export interface IChatMessage {
  date: Date;
  files?: any[];
  dateFormate?: string;
  latitude?: number | null;
  longitude?: number | null;
  message: string;
  quote?: boolean | null;
  sender: string;
  type: 'text' | 'file' | 'map' | 'quote';
  avatar?: string;
  reply?: boolean | null;
}
