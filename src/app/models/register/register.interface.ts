export interface IRegisterAccount {
  username: string;
  password: string;
  email: string;
  gender: string;
  birthdate: string;
}
