export interface IAccessToken {
  'access-token': string;
  'token-type': TOKEN_TYPE;
  'created-at': string;
}

export interface ILoginResponse {
  'refresh-token': string;
  'access-token': string;
  'token-type': TOKEN_TYPE;
  'created-at': string;
}

export interface ILoginPayload {
  payload: string;
}

export enum TOKEN_TYPE {
  BEARER = 'Bearer',
}
