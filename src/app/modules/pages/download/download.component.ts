import { Component, OnInit } from '@angular/core';
import { DownloadFacade } from '@controllers/download/download.facade';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  linkFull = '';
  linkPatch = '';

  constructor(
    private downloadFacade: DownloadFacade,
  ) { }

  ngOnInit(): void {
    this.linkFull = this.downloadFacade.getLinkFull();
    this.linkPatch = this.downloadFacade.getLinkPatch();
  }

}
