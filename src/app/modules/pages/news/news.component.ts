import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsComponent implements OnInit {
  posts: any;
  tempDate = new Date();

  constructor(
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(): void {
    this.http.get('assets/data/news.json').subscribe(
      res => {
        this.posts = res;
        this.cdr.detectChanges();
      }
    );
  }

}
