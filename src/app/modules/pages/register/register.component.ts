import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { debounce } from 'rxjs/operators';
import { interval } from 'rxjs';
import { IRegisterAccount } from '@models/register/register.interface';
import { RegisterFacade } from '@controllers/register/register.facade';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required, Validators.minLength(6)]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    repassword: ['', Validators.required],
    email: [''],
    gender: ['M'],
    birthdate: [new Date(), Validators.required],
  });
  isAgreeWithOurPolicy = false;

  constructor(
    private formBuilder: FormBuilder,
    private registerFacade: RegisterFacade
  ) { }

  ngOnInit(): void {
    // this.registerForm.valueChanges.pipe(
    //   debounce(
    //     () => interval(1000)
    //   )
    // ).subscribe(
    //   (value) => {
    //     console.log(value);
    //   }
    // );
  }

  onSubmit(): void {
    let registerAccountCtx: IRegisterAccount;
    registerAccountCtx = {
      username: this.username.value,
      password: this.password.value,
      email: this.email.value,
      gender: this.gender.value,
      birthdate: this.birthdate.value.toISOString(),
    };
    console.log(registerAccountCtx);
    this.registerFacade.registerNewAccount(registerAccountCtx);
  }

  get username(): AbstractControl {
    return this.registerForm.get('username');
  }

  get password(): AbstractControl {
    return this.registerForm.get('password');
  }

  get repassword(): AbstractControl {
    return this.registerForm.get('repassword');
  }

  get email(): AbstractControl {
    return this.registerForm.get('email');
  }

  get gender(): AbstractControl {
    return this.registerForm.get('gender');
  }

  get birthdate(): AbstractControl {
    return this.registerForm.get('birthdate');
  }

}
