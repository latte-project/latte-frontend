import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { RegisterComponent } from './register/register.component';
import { DownloadComponent } from './download/download.component';
import { InformationComponent } from './information/information.component';
import { SupportComponent } from './support/support.component';
import { NewsComponent } from './news/news.component';
import { ForumComponent } from './forum/forum.component';
import { WikiComponent } from './wiki/wiki.component';
import { LoginComponent } from './login/login.component';
import { NbInputModule, NbCardModule, NbRadioModule, NbDatepickerModule, NbButtonModule, NbToggleModule, NbListModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { SharedComponentsModule } from '@app/shared/components/shared-components.module';
import { ProfileComponent } from './profile/profile.component';
import { ArticleModule } from './article/article.module';


@NgModule({
  declarations: [
    HomepageComponent,
    RegisterComponent,
    DownloadComponent,
    InformationComponent,
    SupportComponent,
    NewsComponent,
    ForumComponent,
    WikiComponent,
    LoginComponent,
    ProfileComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    NbInputModule,
    NbCardModule,
    NbRadioModule,
    NbDatepickerModule,
    NbButtonModule,
    NbToggleModule,
    ReactiveFormsModule,
    NbDateFnsDateModule,
    NbListModule,
    SharedComponentsModule,
    ArticleModule
  ]
})
export class PagesModule { }
