import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleRoutingModule } from './article-routing.module';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { NbCardModule, NbInputModule, NbCheckboxModule } from '@nebular/theme';
import { CreateArticleComponent } from './create-article/create-article.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ArticleDetailComponent,
    CreateArticleComponent
  ],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    NbCardModule,
    CKEditorModule,
    NbInputModule,
    NbCheckboxModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ArticleModule { }
