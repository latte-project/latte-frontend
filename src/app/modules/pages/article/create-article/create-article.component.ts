import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ArticleFacade } from '@controllers/article/article.facade';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { FormGroup, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateArticleComponent implements OnInit {
  public Editor = DecoupledEditor;
  public editorData = '<p>Hello, world</p>';

  constructor(
    private articleFacade: ArticleFacade
  ) { }

  ngOnInit(): void {
  }

  public onReady(editor): void {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  get createArticleForm(): FormGroup {
    return this.articleFacade.getCreateArticleForm();
  }

  onSubmit(): void {
    this.createArticleForm.patchValue({ content: this.editorData });
    console.log(this.createArticleForm.value);
  }

  get tags(): AbstractControl {
    return this.createArticleForm.get('tags');
  }
}
