import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/login/authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService
    ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const accessToken = this.authenticationService.accessTokenValue;
      if (accessToken && accessToken['access-token']) {
          request = request.clone({
              setHeaders: {
                  Authorization: `Bearer ${accessToken['access-token']}`
              }
          });
      }

      return next.handle(request);
  }
}
