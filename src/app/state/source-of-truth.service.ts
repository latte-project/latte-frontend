import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SourceOfTruthStore } from './source-of-truth.store';
import { API } from '../constants/api-endpoint';
import { IAccessToken } from '@app/models/login/login.interface';

@Injectable({ providedIn: 'root' })
export class SourceOfTruthService {

  constructor(
    private sourceOfTruthStore: SourceOfTruthStore,
    private http: HttpClient,
  ) {}

  async init(): Promise<string> {
    await this.updateApiContext();
    await this.updateApiEndpoint();
    console.log(`Source of Truth ran.`);
    return `Source of Truth ran.`;
  }

  async updateApiContext(): Promise<void> {
    try {
      this.sourceOfTruthStore.setLoading(true);

      const URI = API.API_CONTEXT;
      const apiContext = await this.http.get(URI).toPromise();
      this.sourceOfTruthStore.update({ apiContext });

      this.sourceOfTruthStore.setLoading(false);
    } catch (error) {
      this.sourceOfTruthStore.setError(error);
    }
  }

  async updateApiEndpoint(): Promise<void> {
    try {
      this.sourceOfTruthStore.setLoading(true);

      const URI = API.API_ENDPOINT;
      const apiEndpoint = await this.http.get(URI).toPromise();
      this.sourceOfTruthStore.update({ apiEndpoint });

      this.sourceOfTruthStore.setLoading(false);
    } catch (error) {
      this.sourceOfTruthStore.setError(error);
    }
  }

  updateAccessToken(accessToken: IAccessToken): void {
    try {
      this.sourceOfTruthStore.setLoading(true);
      this.sourceOfTruthStore.update({ accessToken });
      this.sourceOfTruthStore.setLoading(false);
    } catch (error) {
      this.sourceOfTruthStore.setError(error);
    }
  }

  updateUserInfo(userInfo: any): void {
    try {
      this.sourceOfTruthStore.setLoading(true);
      this.sourceOfTruthStore.update({ userInfo });
      this.sourceOfTruthStore.setLoading(false);
    } catch (error) {
      this.sourceOfTruthStore.setError(error);
    }
  }
}
