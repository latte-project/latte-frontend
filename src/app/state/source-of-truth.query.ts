import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SourceOfTruthStore, SourceOfTruthState } from './source-of-truth.store';

@Injectable({ providedIn: 'root' })
export class SourceOfTruthQuery extends Query<SourceOfTruthState> {

  constructor(protected store: SourceOfTruthStore) {
    super(store);
  }

}
