import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { IAccessToken } from '@app/models/login/login.interface';

export interface SourceOfTruthState {
  apiContext: any | null;
  apiEndpoint: any | null;
  navbar: any | null;
  accessToken: IAccessToken | null;
  userInfo: any | null;
}

export function createInitialState(): SourceOfTruthState {
  return {
    apiContext: null,
    apiEndpoint: null,
    navbar: null,
    accessToken: null,
    userInfo: null,
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'sourceOfTruth' })
export class SourceOfTruthStore extends Store<SourceOfTruthState> {

  constructor() {
    super(createInitialState());
  }

}

