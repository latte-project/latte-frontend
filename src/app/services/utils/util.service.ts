import { Injectable } from '@angular/core';
import { SourceOfTruthState } from '@app/state/source-of-truth.store';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';

export enum API_TYPE {
  LOGIN = 'login',
  LOGOUT = 'logout',
  ACCESS_TOKEN = 'access-token',
  USER_INFORMATION = 'user-info',
  USER_REGISTER = 'user-register'
}

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  sotState: SourceOfTruthState;

  constructor(
    private sotQuery: SourceOfTruthQuery,
  ) { }

  getApiUrl(key: API_TYPE): string {
    this.sotState = this.sotQuery.getValue();
    const ORIGIN = this.sotState?.apiEndpoint?.ORIGIN;
    let PATH: string;
    switch (key) {
      case API_TYPE.LOGIN:
        PATH = this.sotState?.apiEndpoint?.USER?.LOGIN;
        break;
      case API_TYPE.LOGOUT:
        PATH = this.sotState?.apiEndpoint?.USER?.LOGOUT;
        break;
      case API_TYPE.ACCESS_TOKEN:
        PATH = this.sotState?.apiEndpoint?.USER?.ACCESS_TOKEN;
        break;
      case API_TYPE.USER_INFORMATION:
        PATH = this.sotState?.apiEndpoint?.USER?.USER_INFORMATION;
        break;
      case API_TYPE.USER_REGISTER:
        PATH = this.sotState?.apiEndpoint?.USER?.USER_REGISTER;
        break;
      default:
        PATH = null;
        break;
    }
    if (!ORIGIN || !PATH) {
      return;
    }
    const URL = ORIGIN + PATH;
    return URL;
  }
}
