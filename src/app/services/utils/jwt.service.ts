import { Injectable } from '@angular/core';
import * as jwt from 'jsonwebtoken';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor() { }

  getToken(payload: any, secretKey: string = 'latte'): string {
    return jwt.sign(payload, secretKey);
  }

  encode(payload: any, secretKey: string = 'latte'): string {
    return jwt.sign(payload, secretKey);
  }
}
