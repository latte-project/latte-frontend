import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ILoginResponse, IAccessToken } from '@app/models/login/login.interface';
import { SourceOfTruthState } from '@app/state/source-of-truth.store';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';
import { Observable } from 'rxjs';
import { UtilService, API_TYPE } from '../utils/util.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  sotState: SourceOfTruthState;

  constructor(
    private sotQuery: SourceOfTruthQuery,
    private http: HttpClient,
    private utilService: UtilService,
  ) { }

  login(payload: string): Observable<ILoginResponse> {
    this.sotState = this.sotQuery.getValue();
    const URL = this.utilService.getApiUrl(API_TYPE.LOGIN);
    if (!URL) { return; }
    return this.http.post<ILoginResponse>(URL, { payload }, httpOptions);
  }

  logout(): Observable<any> {
    this.sotState = this.sotQuery.getValue();
    const URL = this.utilService.getApiUrl(API_TYPE.LOGOUT);
    if (!URL) { return; }
    return this.http.get(URL);
  }

  getAccessToken(): Observable<IAccessToken> {
    this.sotState = this.sotQuery.getValue();
    const URL = this.utilService.getApiUrl(API_TYPE.ACCESS_TOKEN);
    console.log(URL);
    if (!URL) { return; }
    return this.http.get<IAccessToken>(URL);
  }

  getUserInfo(): Observable<any> {
    this.sotState = this.sotQuery.getValue();
    const URL = this.utilService.getApiUrl(API_TYPE.USER_INFORMATION);
    console.log(URL);
    if (!URL) { return; }
    return this.http.get(URL);
  }
}
