import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IAccessToken } from '../../models/login/login.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  accessTokenSubject$: BehaviorSubject<any>;
  accessToken$: Observable<any>;

  constructor() {
    this.accessTokenSubject$ = new BehaviorSubject(null);
    this.accessToken$ = this.accessTokenSubject$.asObservable();
  }

  updateAccessToken(accessToken: IAccessToken): void {
    this.accessTokenSubject$.next(accessToken);
  }

  get accessTokenValue(): IAccessToken | null {
    return this.accessTokenSubject$.value;
  }
}
