import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  sot$: Observable<any>;

  constructor(
    private sourceOfTruthQuery: SourceOfTruthQuery,
  ) {
    this.sot$ = this.sourceOfTruthQuery.select();
  }

  getNavbarMeta(): Observable<any> {
    return this.sot$.pipe(
      map((res) => res?.apiContext?.API?.SERVICE?.NAVBAR?.METADATA),
      catchError((err) => {
        console.error(err);
        return of([]);
      }),
    );
  }


}
