import { Injectable } from '@angular/core';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';
import { SourceOfTruthState } from '@app/state/source-of-truth.store';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {
  sourceOfTruth: SourceOfTruthState;
  downloadMeta: any;

  constructor(
    private sourceOfTruthQuery: SourceOfTruthQuery
  ) {
    this.sourceOfTruth = this.sourceOfTruthQuery.getValue();
    this.downloadMeta = this.getDownloadMetadata();
    console.log(this.downloadMeta);
  }

  getDownloadMetadata(): any {
    return this.sourceOfTruth.apiContext?.API?.SERVICE?.DOWNLOAD?.METADATA;
  }

  getLinkFull(): string {
    return this.downloadMeta['link-full'];
  }

  getLinkPatch(): string {
    return this.downloadMeta['link-patch'];
  }
}
