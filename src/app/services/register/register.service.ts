import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SourceOfTruthQuery } from '@app/state/source-of-truth.query';
import { SourceOfTruthState } from '@app/state/source-of-truth.store';
import { UtilService, API_TYPE } from '../utils/util.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  sourceOfTruth: SourceOfTruthState;
  registerMeta: any;
  sotState: SourceOfTruthState;

  constructor(
    private http: HttpClient,
    private sotQuery: SourceOfTruthQuery,
    private utilService: UtilService,
  ) {}

  registerNewAccount(payload: string): Observable<any> {
    this.sotState = this.sotQuery.getValue();
    const URL = this.utilService.getApiUrl(API_TYPE.USER_REGISTER);
    if (!URL) { return; }
    return this.http.post<Observable<any>>(URL, { payload }, this.httpOptions);
  }
}
