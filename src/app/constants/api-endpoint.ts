const PROTOCOL = `http`;
const HOST = `localhost`;
const PORT = `4001`;
const SERVICE = `service`;
const ORIGIN = `${PROTOCOL}://${HOST}:${PORT}`;

export const API = {
  API_COOKIE: `${ORIGIN}`,
  API_ENDPOINT: `${ORIGIN}/api/api-endpoint`,
  API_CONTEXT: `${ORIGIN}/api/api-context`,
  SERVICE: {
    NAVBAR: `${ORIGIN}/${SERVICE}/navbar`,
  }
};
